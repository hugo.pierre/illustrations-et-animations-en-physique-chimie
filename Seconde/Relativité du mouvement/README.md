Je projette aux élèves le gif Manège.gif (source oubliée), pour enclencher la discussion sur le mouvement à priori étrange de la balle.

L'objectif est d'amener les élèves à réaliser que si un mouvement peut-être décrit dans n'importe quel référentiel, il y en a pour lesquels c'est plus simple : je leur propose le modèle Manège.avi et explique ensuite comment j'effectue le changement de référentiel, avec le programme linux script.sh, observable "en directsous windows" avec la vidéo script.mp4 :
* j'extrais toutes les images de la vidéo,
* puis je leur fait subir une rotation afin que la fleur (le réferentiel terrestre) soit en position fixe en haut des images, 
* puis je réassemble les images dans la vidéo Manège-corr.avi, que je projette ensuite. 


