#!/bin/bash

ffmpeg -i $1.avi -vf fps=10 $1-%d.png

for ((i=1;i<=$(ls *.png |wc -l);i++)) 
do
	mogrify -distort SRT "-$(echo "($i-1)*7.5" | bc)" $1-$i.png
done

ffmpeg -framerate 10 -i $1-%d.png -vcodec mpeg4 $1-corr.avi
