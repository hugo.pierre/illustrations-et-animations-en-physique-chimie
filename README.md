# Illustrations et animations en physique-chimie

Vous pouvez trouver ici diverses illustrations au format svg et animations au format html, créées à partir de fichiers svg grâce à l'excellent [Sozi](https://github.com/sozi-projects/Sozi).

* Il s'agit pour l'essentiel de supports niveau lycée.

* les animations ne se suffisent en général pas à elles-mêmes, car elles ont vocation a être utilisées en vidéoprojection pendant mes cours : beaucoup passe par l'oral.

je considère tout ce qui est publié ici plus ou moins dans le domaine public (licence CC0).
